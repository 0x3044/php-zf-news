-- Themes
CREATE TABLE IF NOT EXISTS theme (
  id INT AUTO_INCREMENT PRIMARY KEY,
  title TINYTEXT NOT NULL
);

-- News
CREATE TABLE IF NOT EXISTS news (
  id          INT AUTO_INCREMENT PRIMARY KEY,
  themeId     INT      NOT NULL REFERENCES theme (id),
  dateCreated DATE     NOT NULL,
  title       TINYTEXT NOT NULL,
  content     TEXT
);

-- News foreign key
CREATE INDEX news_themeid_fk1 ON news (themeId);

-- Categories(by date)
CREATE VIEW news_category_by_month AS
SELECT count(*) as news, date_format(dateCreated, '%m-%Y') as period
FROM news
GROUP BY period

-- Categories (by themeId)
CREATE VIEW news_category_by_theme AS
SELECT count(*) as news, t.id as themeId, t.title as theme
FROM news n
left join theme t on n.themeId = t.id
GROUP BY n.themeId

-- Test data/themes
INSERT INTO theme (title) VALUES ('Irelia'), ('Lulu'), ('Evelynn'), ('Annie'), ('Ahri');

-- Test data/news
INSERT INTO news (themeId, dateCreated, title) VALUES
(1, '2013-11-21', 'TSM Bjergsen 1v5 Ranked 5s (REAL)'),
(1, '2013-11-21', 'How TO make Lulu cupcakes ft.Dyrus ! '),
(2, '2013-10-21', 'Preseason Leagues Improvements ON PBE'),
(1, '2013-10-21', 'Doublelift learns TO play Jinx (funny)'),
(3, '2013-09-21', 'TSM Bjergsen Interview-Thoughts AND Expectations of TSM '),
(2, '2013-08-21', 'Nocturne''s Q Ninja Nerfed'),
(4, '2013-05-21', 'I was scammed BY organization ON Riot official challenger EVENT. '),
(1, '2012-11-21', 'QUICK UPDATE ON those new game queues ON the PBE '),
(1, '2012-11-21', 'A Chrome addon TO fix twitch lag BY-Redirects EU connections TO the US '),
(2, '2012-11-21', 'Calling ALL Pros, Semi-Pros, AND Retirees ! '),
(2, '2012-07-21', 'Minions AND Monsters need VU');

UPDATE news
SET content = 'Popstar Ahris record smashing single, “Charmd” shot up the charts AND fueled GLOBAL excitement FOR her upcoming Spirit Rush Tour.The newly minted star was recently snapped practicing fresh dance moves, AND WITH her stage SHOW rumored TO include a dramatic entrance AND EXIT, NOT TO mention melodic new particles AND a rhythmically-inclined Spirit Orb, Popstar Ahris sure TO take the world BY storm.' WHERE id > 0;