<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Application\Model\Entity\News;
use Application\Model\View\News\DateRange as NewsDateRangeView;
use Application\Model\View\News\Theme as NewsThemeView;
use Application\Model\Table\NewsTable;
use Application\Model\Entity\Theme;
use Application\Model\Table\ThemeTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Mvc\Application;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{
    /**
     * @param MvcEvent $e
     */
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    /**
     * @return array
     */
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                // news_category_by_month
                'Application\Model\View\News\DateRange' =>  function($sm) {
                        $tableGateway = $sm->get('NewsDateRangeViewTableGateway');
                        $table = new NewsDateRangeView($tableGateway);
                        return $table;
                    },
                'NewsDateRangeViewTableGateway' => function ($sm) {
                        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                        $resultSetPrototype = new ResultSet();
                        return new TableGateway('news_category_by_month', $dbAdapter, null, $resultSetPrototype);
                    },
                // news_category_by_theme
                'Application\Model\View\News\Theme' =>  function($sm) {
                        $tableGateway = $sm->get('NewsThemeViewTableGateway');
                        $table = new NewsThemeView($tableGateway);
                        return $table;
                    },
                'NewsThemeViewTableGateway' => function ($sm) {
                        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                        $resultSetPrototype = new ResultSet();
                        return new TableGateway('news_category_by_theme', $dbAdapter, null, $resultSetPrototype);
                    },

                // news
                'Application\Model\Table\NewsTable' =>  function($sm) {
                        $tableGateway = $sm->get('NewsTableGateway');
                        $table = new NewsTable($tableGateway);
                        return $table;
                    },
                'NewsTableGateway' => function ($sm) {
                        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                        $resultSetPrototype = new ResultSet();
                        $resultSetPrototype->setArrayObjectPrototype(new News());
                        return new TableGateway('news', $dbAdapter, null, $resultSetPrototype);
                    },

                // theme
                'Application\Model\Table\ThemeTable' =>  function($sm) {
                        $tableGateway = $sm->get('ThemeTableGateway');
                        $table = new ThemeTable($tableGateway);
                        return $table;
                    },
                'ThemeTableGateway' => function ($sm) {
                        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                        $resultSetPrototype = new ResultSet();
                        $resultSetPrototype->setArrayObjectPrototype(new Theme());
                        return new TableGateway('Theme', $dbAdapter, null, $resultSetPrototype);
                    },
            )
        );
    }
}
