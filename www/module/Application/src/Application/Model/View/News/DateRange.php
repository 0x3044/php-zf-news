<?php
namespace Application\Model\View\News;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

class DateRange
{
    /**
     * @var \Zend\Db\TableGateway\TableGateway
     */
    protected $tableGateway;

    /**
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * Fetch all
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetchAll()
    {
        $select = $this->tableGateway->select();

        return $select;
    }

    /**
     * Return categories as array
     * @return array
     */
    public function fetchCategories()
    {
        $categories = array();
        $resultSet = $this->tableGateway->select();

        foreach($resultSet as $record) {
            $period = explode('-', $record['period']);
            $pMonth = (int) $period[0];
            $pYear = (int) $period[1];

            $categories[$pYear][$pMonth] = $record['news'];
        }

        return $categories;
    }
}