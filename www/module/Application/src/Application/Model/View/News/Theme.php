<?php
namespace Application\Model\View\News;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

class Theme
{
    /**
     * @var \Zend\Db\TableGateway\TableGateway
     */
    protected $tableGateway;

    /**
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * Fetch all
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetchAll()
    {
        $select = $this->tableGateway->select();

        return $select;
    }

    /**
     * Fetch categories
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetchCategories()
    {
        return $this->fetchAll();
    }
}