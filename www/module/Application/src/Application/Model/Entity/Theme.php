<?php
namespace Application\Model\Entity;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

class Theme
{
    public $id;
    public $title;

    /**
     * @param $data
     */
    public function exchangeArray($data)
    {
        foreach (array('id', 'title') as $fieldName) {
            $this->$fieldName = (!empty($data[$fieldName])) ? $data[$fieldName] : null;
        }
    }
}