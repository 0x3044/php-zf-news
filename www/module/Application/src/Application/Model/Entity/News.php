<?php
namespace Application\Model\Entity;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;

/**
 * Class News
 * @package Application\Model
 */
class News
{
    public $id;
    public $themeId;
    public $theme;
    public $dateCreated;
    public $title;
    public $content;
    public $num;

    /**
     * @var InputFilterInterface
     */
    protected $inputFilter;

    /**
     * @param $data
     */
    public function exchangeArray($data)
    {
        foreach (array('id', 'themeId', 'theme', 'dateCreated', 'title', 'content', 'num') as $fieldName) {
            $this->$fieldName = (!empty($data[$fieldName])) ? $data[$fieldName] : null;
        }
    }

    /**
     * @return string
     */
    public function getContentPreview() {
        return substr($this->content, 0, 256);
    }

    /**
     * @return bool
     */
    public function hasShortContent() {
        return strlen($this->content) > 256;
    }

    /**
     * @param InputFilterInterface $inputFilter
     * @throws \Exception
     */
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    /**
     * Validation
     * @return InputFilter
     */
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $inputFilter->add(array(
                'name'     => 'id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            ));

            $inputFilter->add(array(
                'name'     => 'title',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),
            ));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    /**
     * @return array
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}