<?php
namespace Application\Model\Table;

use Application\Model\Builder\NewsTable\FromNewsSpec;
use Application\Model\Entity\News;
use Application\Model\Filter\NewsSpec;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

/**
 * Class NewsTable
 * @package News\Model
 */
class NewsTable
{
    /**
     * @var \Zend\Db\TableGateway\TableGateway
     */
    protected $tableGateway;

    /**
     * @return \Zend\Db\TableGateway\TableGateway
     */
    public function getTableGateway()
    {
        return $this->tableGateway;
    }

    /**
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetchAll()
    {
        $select = $this->tableGateway->select(function(Select $select) {
            $select
                ->order('dateCreated desc')
                ->join('theme', 'news.themeId = theme.id', array('theme' => 'title'));
        });

        return $select;
    }

    /**
     * Fetch news (filtered)
     * @param $offset
     * @param $limit
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetchLimited($offset, $limit)
    {
        $select = $this->tableGateway->select(function(Select $select) use($offset, $limit) {
            $select
                ->join('theme', 'news.themeId = theme.id', array('theme' => 'title'))
                ->order('dateCreated desc')
                ->offset($offset)
                ->limit($limit);
        });

        return $select;
    }

    /**
     * Setup query
     * @param Select $select
     * @param NewsSpec $newsSpec
     * @param bool $ignoreLimit
     */
    public function specSelect(Select $select, NewsSpec $newsSpec, $ignoreLimit = false) {

    }

    /**
     * Fetch news(by spec)
     * @param NewsSpec $newsSpec
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetchFiltered(NewsSpec $newsSpec)
    {
        $builder = new FromNewsSpec($this, $newsSpec);
        return $builder->build();
    }

    /**
     * Return the numbers of news
     * @param NewsSpec $newsSpec
     * @return int
     */
    public function getNewsCount(NewsSpec $newsSpec) {
        $sThis = $this;

        $select = $this->tableGateway->select(function(Select $select) use($newsSpec, $sThis) {
            $select->columns(array('num' => new \Zend\Db\Sql\Predicate\Expression('count(*)')));
            $sThis->specSelect($select, $newsSpec, true);
        });

        return $select->current()->num;
    }

    /**
     * @param $id
     * @return array|\ArrayObject|null
     * @throws \Exception
     */
    public function getNews($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(function(Select $select) use ($id) {
            $select->where(array('id' => $id));
        });

        $row = $rowset->current();

        if (!$row) {
            throw new \Exception("Could not find row $id");
        }

        return $row;
    }

    /**
     * @param News $news
     * @throws \Exception
     */
    public function saveNews(News $news)
    {
        $data = array(
            'title'  => $news->title,
            'dateCreated' => $news->dateCreated,
            'themeId' => $news->themeId,
            'content' => $news->content
        );

        $id = (int) $news->id;

        if ($id == 0) {
            $this->tableGateway->insert($data);
        }else{
            if ($this->getNews($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            }else{
                throw new \Exception('News id does not exist');
            }
        }
    }

    /**
     * @param $id
     * @return int
     */
    public function deleteNews($id)
    {
        return $this->tableGateway->delete(array('id' => (int) $id));
    }
}