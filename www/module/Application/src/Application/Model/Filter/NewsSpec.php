<?php
namespace Application\Model\Filter;

/**
 * Class NewsSpec
 * Filter specs for client/news/index
 */
class NewsSpec
{
    /**
     * @var string Category
     */
    protected $category;

    /**
     * @var int Offset
     */
    protected $offset;

    /**
     * @var int Limit
     */
    protected $limit;

    /**
     * @var int Theme
     */
    protected $themeId;

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = (int) $limit;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $offset
     */
    public function setOffset($offset)
    {
        $this->offset =(int)  $offset;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @param int $themeId
     */
    public function setThemeId($themeId)
    {
        $this->themeId = (int) $themeId;
    }

    /**
     * @return int
     */
    public function getThemeId()
    {
        return $this->themeId;
    }

    /**
     * NewsSpec
     * @param array $options
     * @throws \Exception
     */
    public function __construct(Array $options) {
        foreach ($options as $name => $value) {
            switch($name) {
                default:
                    throw new \Exception("Unknown spec {$name}");

                case 'category':
                    $this->setCategory($value);
                    break;

                case 'offset':
                    $this->setOffset($value);
                    break;

                case 'limit':
                    $this->setLimit($value);
                    break;

                case 'themeId':
                    $this->setThemeId($value);
                    break;
            }
        }
    }

    /**
     * Return true if limit specified
     * @return bool
     */
    public function hasLimit() {
        return !is_null($this->limit);
    }

    /**
     * Return true if category specified
     * @return bool
     */
    public function hasCategory() {
        return !is_null($this->category);
    }

    /**
     * Return true if themeId specified
     * @return bool
     */
    public function hasThemeId() {
        return !is_null($this->themeId) && $this->themeId>0;
    }

    /**
     * Return category metadata
     * @return array
     * @throws \Exception
     */
    public function getCategoryMetadata() {
        $metadata = array();
        $category = $this->getCategory();

        if ($category>=1900 && $category<=9999) {
            $metadata = array(
                'type' => 'year',
                'year' => $category
            );
        }else if(preg_match('/[0-9]{1,2}-[0-9]{4}/', $category)) {
            $dateFormat = explode('-', $category);
            $metadata = array(
                'type' => 'month',
                'month' => $dateFormat[0],
                'year' => $dateFormat[1]
            );
        }

        if ($this->hasThemeId()) {
            $metadata['type'] = 'themeId';
            $metadata['themeId'] = $this->getThemeId();
        }

        if (!isset($metadata['type']) || !$metadata['type']) {
            throw new \Exception('Unknown category');
        }

        return $metadata;
    }
}