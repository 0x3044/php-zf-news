<?php
namespace Application\Model\Builder\NewsTable;

use Application\Model\Filter\NewsSpec;
use Application\Model\Table\NewsTable;
use Zend\Db\Sql\Select;

/**
 * Class FromNewsSpec
 * @package Application\Model\Builder
 */
class FromNewsSpec
{
    /**
     * @var NewsTable
     */
    protected $newsTable;

    /**
     * @var NewsSpec
     */
    protected $newsSpec;

    /**
     * @var bool If true ignore limit spec
     */
    protected $ignoreLimit = false;

    /**
     * @param \Application\Model\Table\NewsTable $newsTable
     */
    public function setNewsTable($newsTable)
    {
        $this->newsTable = $newsTable;
    }

    /**
     * @return \Application\Model\Table\NewsTable
     */
    public function getNewsTable()
    {
        return $this->newsTable;
    }

    /**
     * @param NewsSpec $newsSpec
     */
    public function setNewsSpec(NewsSpec $newsSpec)
    {
        $this->newsSpec = $newsSpec;
    }

    /**
     * @return NewsSpec
     */
    public function getNewsSpec()
    {
        return $this->newsSpec;
    }

    /**
     * @param boolean $ignoreLimit
     */
    public function setIgnoreLimit($ignoreLimit)
    {
        $this->ignoreLimit = $ignoreLimit;
    }

    /**
     * @return boolean
     */
    public function getIgnoreLimit()
    {
        return $this->ignoreLimit;
    }

    /**
     * @param NewsTable $newsTable
     * @param NewsSpec $newsSpec
     */
    public function __construct(NewsTable $newsTable, NewsSpec $newsSpec)
    {
        $this->setNewsTable($newsTable);
        $this->setNewsSpec($newsSpec);
    }

    /**
     * Build select query from newsSpec
     * @return Select
     */
    public function build() {
        $newsTable = $this->getNewsTable();
        $newsSpec = $this->getNewsSpec();
        $sThis = $this;

        $select = $newsTable->getTableGateway()->select(function(Select $select) use($sThis) {
            $sThis->setupQuery($select);
        });

        return $select;
    }

    /**
     * Setup query
     * @param Select $select
     * @return bool
     */
    public function setupQuery(Select $select) {
        $where = array();
        $newsSpec = $this->getNewsSpec();

        $select
            ->join('theme', 'news.themeId = theme.id', array('theme' => 'title'))
            ->order('dateCreated desc');

        if ($newsSpec->hasCategory()) {
            $categoryMetadata = $newsSpec->getCategoryMetadata();

            switch ($categoryMetadata['type']) {
                case 'year':
                    $where = array(
                        new \Zend\Db\Sql\Predicate\Expression('YEAR(dateCreated) = "'.$categoryMetadata['year'].'"'),
                    );
                    break;

                case 'month':
                    $where = array(
                        new \Zend\Db\Sql\Predicate\Expression('YEAR(dateCreated) = "'.$categoryMetadata['year'].'"'),
                        new \Zend\Db\Sql\Predicate\Expression('MONTH(dateCreated) = "'.$categoryMetadata['month'].'"')
                    );
                    break;
            }
        }

        if ($newsSpec->hasThemeId()) {
            $where = array_merge($where, array(
                'themeId' => $newsSpec->getThemeId()
            ));
        }

        if (!$this->getIgnoreLimit() && $newsSpec->hasLimit()) {
            $select->offset($newsSpec->getOffset());
            $select->limit($newsSpec->getLimit());
        }

        if (count($where)) {
            $select->where($where);
        }

        return true;
    }
}