<?php
$theme_rules = '[0-9]+';
$category_rules = '([0-9]{1,2}-[0-9]{4})|([0-9]{4})';

return array(
    'router' => array(
        'routes' => array(
            'client_news_index' => array(
                'type'    => 'segment',
                'options' => array(
                    'route' => '/client/news/page[/][:page][/]',
                    'constraints' => array(
                        'id'     => '[0-9]+',
                        'page'   => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Client\Controller\News',
                        'action'     => 'index',
                        'page' => 1
                    ),
                ),
            ),
            'client_news_index_category' => array(
                'type'    => 'segment',
                'options' => array(
                    'route' => '/client/news/[:category][/]',
                    'constraints' => array(
                        'id'     => '[0-9]+',
                        'page'   => '[0-9]+',
                        'category' => $category_rules
                    ),
                    'defaults' => array(
                        'controller' => 'Client\Controller\News',
                        'action'     => 'index',
                        'page' => 1
                    ),
                ),
            ),
            'client_news_index_category_page' => array(
                'type'    => 'segment',
                'options' => array(
                    'route' => '/client/news/[:category][/]page[/][:page][/]',
                    'constraints' => array(
                        'id'     => '[0-9]+',
                        'page'   => '[0-9]+',
                        'category' => $category_rules
                    ),
                    'defaults' => array(
                        'controller' => 'Client\Controller\News',
                        'action'     => 'index',
                        'page' => 1
                    ),
                ),
            ),
            'client_news_index_theme' => array(
                'type'    => 'segment',
                'options' => array(
                    'route' => '/client/news/theme/[:themeId][/]',
                    'constraints' => array(
                        'id'     => '[0-9]+',
                        'page'   => '[0-9]+',
                        'themeId'  => $theme_rules
                    ),
                    'defaults' => array(
                        'controller' => 'Client\Controller\News',
                        'action'     => 'index',
                        'page' => 1
                    ),
                ),
            ),
            'client_news_index_theme_page' => array(
                'type'    => 'segment',
                'options' => array(
                    'route' => '/client/news/theme/[:themeId][/]page[/][:page][/]',
                    'constraints' => array(
                        'id'     => '[0-9]+',
                        'page'   => '[0-9]+',
                        'themeId'  => $theme_rules
                    ),
                    'defaults' => array(
                        'controller' => 'Client\Controller\News',
                        'action'     => 'index',
                        'page' => 1
                    ),
                ),
            ),
            'client_news' => array(
                'type'    => 'segment',
                'options' => array(
                    'route' => '/client/news[/][:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+'
                    ),
                    'defaults' => array(
                        'controller' => 'Client\Controller\News',
                        'action'     => 'index',
                    ),
                ),
            )
        )
    ),

    'controllers' => array(
        'invokables' => array(
            'Client\Controller\News' => 'Client\Controller\NewsController',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'client' => __DIR__ . '/../view',
        ),
    ),
);