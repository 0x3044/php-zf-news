<?php
namespace Client;

/**
 * Class Module
 * @package Album
 */
class Module
{
    /**
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * @return array
     */
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Client\View\News\Category\DateRange' => function($sm) {
                        $viewTheme = $sm->get('Application\Model\View\News\DateRange');
                        return new \Client\View\News\Category\DateRange($viewTheme);
                    },
                'Client\View\News\Category\Theme' => function($sm) {
                        $viewTheme = $sm->get('Application\Model\View\News\Theme');
                        return new \Client\View\News\Category\Theme($viewTheme);
                    },
            )
        );
    }
}