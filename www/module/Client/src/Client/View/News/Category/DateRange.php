<?php
namespace Client\View\News\Category;

use Application\Model\View\News\DateRange as DateRangeView;

/**
 * Class DateRange
 * @package Client\View
 */
class DateRange
{
    /**
     * @var DateRangeView
     */
    protected $dateRangeView;

    /**
     * @param \Application\Model\View\News\DateRange $dateRangeView
     */
    public function setDateRangeView($dateRangeView)
    {
        $this->dateRangeView = $dateRangeView;
    }

    /**
     * @return \Application\Model\View\News\DateRange
     */
    public function getDateRangeView()
    {
        return $this->dateRangeView;
    }

    /**
     * @param DateRangeView $dataRangeView
     */
    public function __construct(DateRangeView $dataRangeView)
    {
        $this->setDateRangeView($dataRangeView);
    }

    /**
     * @return array
     */
    public function getCategories()
    {
        return $this->getDateRangeView()->fetchCategories();
    }
}