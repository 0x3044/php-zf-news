<?php
namespace Client\View\News\Category;

use Application\Model\View\News\Theme as ThemeView;

/**
 * Class DateRange
 * @package Client\View
 */
class Theme
{
    /**
     * @var ThemeView
     */
    protected $themeView;

    /**
     * @param \Application\Model\View\News\Theme $themeView
     */
    public function setThemeView(ThemeView $themeView)
    {
        $this->themeView = $themeView;
    }

    /**
     * @return \Application\Model\View\News\DateRange
     */
    public function getThemeView()
    {
        return $this->themeView;
    }

    /**
     * @param ThemeView $themeView
     */
    public function __construct(ThemeView $themeView)
    {
        $this->setThemeView($themeView);
    }

    /**
     * @return array
     */
    public function getCategories()
    {
        return $this->getThemeView()->fetchCategories();
    }
}