<?php
namespace Client\Controller;

use Application\Model\Filter\NewsSpec;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * Class NewsController
 * @package Client\Controller
 */
class NewsController extends AbstractActionController
{
    /**
     * News on page
     * @const int
     */
    const PAGE_SIZE = 5;

    /**
     * @var
     */
    protected $newsTable;

    /**
     * @var NewsSpec
     */
    protected $newsSpec;

    /**
     * Return newsTable
     * @return object
     */
    protected function getNewsTable()
    {
        if (!$this->newsTable) {
            $sm = $this->getServiceLocator();
            $this->newsTable = $sm->get('Application\Model\Table\NewsTable');
        }

        return $this->newsTable;
    }

    /**
     * @return \Client\Model\Filter\NewsSpec
     */
    protected function getNewsSpec()
    {
        if (!$this->newsSpec)
        {
            $page = (int) $this->params()->fromRoute('page', 0) - 1;
            $category = $this->params()->fromRoute('category', null);
            $themeId = $this->params()->fromRoute('themeId', null);

            if (!is_int($page) || $page < 0) {
                $page = 0;
            }

            $this->newsSpec = new NewsSpec(array(
                'limit' => self::PAGE_SIZE,
                'offset' => self::PAGE_SIZE*$page,
                'category' => $category,
                'themeId' => $themeId
            ));
        }

        return $this->newsSpec;
    }

    /**
     * @return array
     */
    protected function getCategoriesMenuViewData()
    {
        $dateRangeMenu = $this->getServiceLocator()->get('Client\View\News\Category\DateRange');
        $themeMenu = $this->getServiceLocator()->get('Client\View\News\Category\Theme');

        return array(
            'categories_by_date' => $dateRangeMenu->getCategories(),
            'categories_by_theme' => $themeMenu->getCategories(),
        );
    }

    /**
     * List of news
     * @return ViewModel
     */
    public function indexAction()
    {
        // query data
        $page = (int) $this->params()->fromRoute('page', 0);
        $category = $this->params()->fromRoute('category', null);

        // newsSpec
        $newsSpec = $this->getNewsSpec($page, $category);

        // news records
        $news_records = $this->getNewsTable()->fetchFiltered($newsSpec);

        // view
        return new ViewModel(array_merge($this->getCategoriesMenuViewData(), array(
            'news' => $news_records,
            'news_count' => ceil($this->getNewsTable()->getNewsCount($newsSpec)/self::PAGE_SIZE),
            'page' => ($page),
            'category' => $category
        )));
    }

    /**
     * Read article
     * @return array|\Zend\Http\Response
     */
    public function viewAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);

        if (!$id) {
            return $this->redirect()->toRoute('admin_news', array(
                'action' => 'create'
            ));
        }

        try {
            $news_record = $this->getNewsTable()->getNews($id);
        }
        catch (\Exception $ex) {
            return $this->redirect()->toRoute('client_news', array(
                'action' => 'index'
            ));
        }

        return new ViewModel(array_merge($this->getCategoriesMenuViewData(), array(
            'id' => $id,
            'news_record' => $news_record,
        )));
    }
}