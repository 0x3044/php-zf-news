<?php
return array(
    'router' => array(
        'routes' => array(
            'admin_news' => array(
                'type'    => 'segment',
                'options' => array(
                    'route' => '/admin/news[/][:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Admin\Controller\News',
                        'action'     => 'index',
                    ),
                ),
            ),
        )
    ),

    'controllers' => array(
        'invokables' => array(
            'Admin\Controller\News' => 'Admin\Controller\NewsController',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'admin' => __DIR__ . '/../view',
        )
    ),
);