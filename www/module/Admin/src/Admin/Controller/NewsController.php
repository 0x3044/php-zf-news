<?php
namespace Admin\Controller;

use Admin\Form\NewsForm;
use Application\Model\Entity\News;
use Application\Model\Table\ThemeTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * Class NewsController
 * CRUD-controller for news model
 * @package News\Controller
 */
class NewsController extends AbstractActionController
{
    /**
     * @var \Application\Model\Table\NewsTable
     */
    protected $newsTable;

    /**
     * Return news table
     * @return mixed
     */
    protected function getNewsTable() {
        if (!$this->newsTable) {
            $sm = $this->getServiceLocator();
            $this->newsTable = $sm->get('Application\Model\Table\NewsTable');
        }

        return $this->newsTable;
    }

    /**
     * Prepare form
     * (Извините, я не хочу здесь использовать Hydrator. Мне лень.)
     * @param NewsForm $form
     */
    protected function prepareForm(NewsForm $form) {
        $themeTable = $this->getServiceLocator()->get('Application\Model\Table\ThemeTable');
        $themesResultSet = $themeTable->fetchAll();
        $themes = array();

        foreach ($themesResultSet as $record) {
            $themes[$record->id] = $record->title;
        }

        $form->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'themeId',
            'options' => array(
                'label' => 'Theme',
                'value_options' => $themes,
            )
        ));
    }

    /**
     * List of news
     * @return array|ViewModel
     */
    public function indexAction() {
        return new ViewModel(array(
            'news' => $this->getNewsTable()->fetchAll(),
        ));
    }

    /**
     * Publish news
     * @return array|\Zend\Http\Response
     */
    public function createAction() {
        $form = new NewsForm();
        $form->get('submit')->setValue('Publish');
        $this->prepareForm($form);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $news = new News();
            $form->setInputFilter($news->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $news->exchangeArray($form->getData());
                $this->getNewsTable()->saveNews($news);

                // Redirect to list of news
                return $this->redirect()->toRoute('admin_news');
            }
        }
        return array('form' => $form);
    }

    /**
     * Edit news
     * @return array|\Zend\Http\Response
     */
    public function editAction() {
        $id = (int) $this->params()->fromRoute('id', 0);

        if (!$id) {
            return $this->redirect()->toRoute('admin_news', array(
                'action' => 'create'
            ));
        }

        // Get the News with the specified id.  An exception is thrown
        // if it cannot be found, in which case go to the index page.
        try {
            $news = $this->getNewsTable()->getNews($id);
        }
        catch (\Exception $ex) {
            return $this->redirect()->toRoute('news', array(
                'action' => 'index'
            ));
        }

        $form  = new NewsForm();
        $this->prepareForm($form);
        $form->bind($news);
        $form->get('submit')->setAttribute('value', 'Save changes');

        $request = $this->getRequest();

        if ($request->isPost()) {
            $form->setInputFilter($news->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->getNewsTable()->saveNews($news);

                // Redirect to list of newss
                return $this->redirect()->toRoute('news');
            }
        }

        return array(
            'id' => $id,
            'form' => $form,
        );
    }

    /**
     * Delete news
     * @return array|\Zend\Http\Response
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);

        if (!$id) {
            return $this->redirect()->toRoute('admin_news');
        }

        $request = $this->getRequest();

        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->getNewsTable()->deleteNews($id);
            }

            // Redirect to list of news
            return $this->redirect()->toRoute('admin_news');
        }

        return array(
            'id'    => $id,
            'news' => $this->getNewsTable()->getNews($id)
        );
    }
}