<?php
namespace Admin\Form;

use Zend\Form\Form;
use Application\Model\ThemeTable;

/**
 * Class NewsForm
 * @package Admin\Form
 */
class NewsForm extends Form
{
    /**
     * @param null $name
     */
    public function __construct($name = null)
    {
        parent::__construct('news');

        $this->setAttribute('class', 'form-horizontal');

        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'name' => 'title',
            'type' => 'Text',
            'options' => array(
                'label' => 'Title',
            ),
        ));
        $this->add(array(
            'name' => 'themeId',
            'type' => 'select',
            'options' => array(
                'label' => 'Theme',
            )
        ));
        $this->add(array(
            'name' => 'dateCreated',
            'type' => 'Text',
            'options' => array(
                'label' => 'Date',
            ),
        ));
        $this->add(array(
            'name' => 'content',
            'type' => 'Textarea',
            'options' => array(
                'label' => 'Content',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));
    }
}